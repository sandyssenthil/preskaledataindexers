package com.preskales.salesforce;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Map.Entry;
import java.util.logging.Logger;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;


public class SalesForceCRM {
	private static boolean proceed = true;
	private static DefaultHttpClient httpclient = new DefaultHttpClient();
	private static int noofRecords = 0;	
	private final static Logger LOGGER = Logger.getLogger(SalesForceCRM.class.getName());
	private static String moduleName = "";
	
	
	private static boolean runSpecific = false;
	private static boolean runForViews = false;
	private static boolean runForData = false;
	private static Map<String, String> modules = new HashMap<String, String>();
	static {
		modules.put("Account", "https://ap16.salesforce.com/services/data/v42.0/query/?q=SELECT+id,IsDeleted,MasterRecordId,Name,Type,ParentId,BillingStreet,BillingCity,BillingState,BillingPostalCode,BillingCountry,ShippingStreet,ShippingCity,ShippingState,ShippingPostalCode,ShippingCountry,Phone,Fax,AccountNumber,Website,Sic,AnnualRevenue,NumberOfEmployees,Ownership,TickerSymbol,Description,Rating,Site,OwnerId,CreatedDate,CreatedById,LastModifiedDate,LastModifiedById,LastActivityDate,CustomerPriority__c,SLA__c,Active__c,NumberofLocations__c,UpsellOpportunity__c,SLASerialNumber__c,SLAExpirationDate__c+from+Account");
	    modules.put("Lead", "https://ap16.salesforce.com/services/data/v42.0/query/?q=SELECT+id,name,email,LastName,FirstName,company,Salutation,phone,Country,address,description,industry,rating,title,website,status,LeadSource,OwnerId,IsConverted,ConvertedDate,ConvertedAccountId,ConvertedContactId,ConvertedOpportunityId,IsUnreadByOwner,CreatedDate,CreatedById,EmailBouncedReason,EmailBouncedDate,ProductInterest__c,Primary__c,CurrentGenerators__c,NumberofLocations__c,annualrevenue,fax,numberofemployees+from+Lead");
	    modules.put("Contact", "https://ap16.salesforce.com/services/data/v42.0/query/?q=SELECT+id,IsDeleted,MasterRecordId,AccountId,LastName,FirstName,Salutation,Name,OtherStreet,OtherCity,OtherState,OtherPostalCode,OtherCountry,MailingStreet,MailingCity,MailingState,MailingPostalCode,MailingCountry,Phone,Fax,MobilePhone,HomePhone,OtherPhone,AssistantPhone,ReportsToId,Email,Title,Department,AssistantName,LeadSource,Birthdate,Description,OwnerId,CreatedDate,CreatedById,LastModifiedDate,LastModifiedById,SystemModstamp,LastActivityDate,LastCURequestDate,LastCUUpdateDate,EmailBouncedReason,EmailBouncedDate,Level__c,Languages__c+from+Contact");
	    modules.put("Opportunity", "https://ap16.salesforce.com/services/data/v42.0/query/?q=SELECT+id,AccountId,Amount,CampaignId,CloseDate,ContactId,ContractId,Description,OtherStreet,ExpectedRevenue,OtherState,ForecastCategory,OtherCountry,ForecastCategoryName,HasOpenActivity,HasOpportunityLineItem,HasOverdueTask,IqScore,IsClosed,IsDeleted,IsWon,LastActivityDate,LastAmountChangedHistoryId,LastCloseDateChangedHistoryId,LeadSource,Name,OwnerId,StageName,AssistantName,LeadSource,Type+from+Opportunity");
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		try {
			LOGGER.info("SalesForceCRM processor started at:-" + new Date());
			for (int i = 0; i < args.length; i++) {
				if (args[i].equals("-Leads")) {
					moduleName = "leads";
				} else if (args[i].equals("-Contacts")) {
					moduleName = "contacts";
				} else if (args[i].equals("-Accounts")) {
					moduleName = "sales_accounts";
				} else if (args[i].equals("-Deals")) {
					moduleName = "deals";
				} else if (args[i].equals("-Sales_activities")) {
					moduleName = "sales_activities";
				} else if (args[i].equals("-runForAll")) {
					// runForAll = true;
				} else if (args[i].equals("-runSpecific")) {
					runSpecific = true;
				} 
				
			}
			
				fetchData();
			
		
			LOGGER.info("SalesForceCRM processor Ended at:-" + new Date());

		} catch (final Exception e) {
			LOGGER.severe("Exception while parsing the main arguments:-" + e.toString());
		}
	}
	
	private static void fetchData() {
		List<JSONArray> docs = new ArrayList<JSONArray>();
		for (Entry<String, String> module : modules.entrySet()) {
			LOGGER.info("processing Module:-" + module.getKey());
			docs.clear();
			proceed = true;
			noofRecords = 0;
			while (proceed) {
				try {
					Scanner sc = APIEndpoint(module.getValue(), "00D2w000009LkM3!AQMAQIBHInvUCtyZGHTsfcWtEHn23Tpk_kpsJk7fUjjrZPwj2wXJuAaGSp5iOUREKjRH8OpD16jBZVw4wwTne7KLAokHRPQR");
					if (sc == null) {
						//throw new Exception("error occured");
					}

					ObjectMapper mapper = new ObjectMapper();

					JSONObject jso_object;
					while (sc.hasNextLine()) {
						String api_content = sc.nextLine();
						Object json = mapper.readValue(api_content, Object.class);
						String indented = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
						jso_object = new JSONObject(indented);
						docs.add(jso_object.getJSONArray("records"));
						// pagination:-
//						noofRecords = noofRecords + jso_object.getJSONArray(filter.getValue()).length();
//						int page = Integer.valueOf(jso_object.getJSONObject("meta").getInt("total_pages"));
//						if (page > current_page_number) {
//							current_page_number++; // proceed next page
//						} else {
//							proceed = false;
//						}
						proceed = false;
					}

				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}

			}
			LOGGER.info("No of records Fetched from api:-" + noofRecords);

		}
		}
	
	
	private static Scanner APIEndpoint(String api_url, String api_token) {
		try {
			System.out.println("Endpoint:"+api_url);
			// Creating a HttpGet object
			HttpGet httpget = new HttpGet(api_url);
	    	 httpget.addHeader("Authorization", "Authorization: Bearer "+api_token);
			// Printing the method used
			LOGGER.info("Request Type: " + httpget.getMethod());
			HttpResponse httpresponse = httpclient.execute(httpget);
			// check the http status
			if (httpresponse.getStatusLine().getStatusCode() != 200) {
				// if response code is not 200, then something happened
				// in the account.need to reinvestigate the account
				// settings.
				LOGGER.info(httpresponse.getStatusLine().toString());
				triggeralert();
			} else {
				LOGGER.info("Response code:-" + httpresponse.getStatusLine().getStatusCode());
				System.out.println(httpresponse.getEntity().getContent());
			}
			Scanner sc = new Scanner(httpresponse.getEntity().getContent());
			return sc;

		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
	public static void triggeralert() {
		// Trigger alert if response is not equal to 200
	}

}
