package com.preskales.freshworks.DBHandlers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;

import org.bson.conversions.Bson;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.model.DBCollectionUpdateOptions;
import com.mongodb.client.model.Filters;
import com.mongodb.util.JSON;
import com.preskales.freshworks.FreshworksCRM;

public class FreshworksCRMDBHandler {
	private static MongoClient mongo;
	private static MongoClientURI uri;
	private static DB database;
	private static DBCollection collection;
	//private static DBCollectionUpdateOptions update_options = new DBCollectionUpdateOptions();
	private final static Logger LOGGER = Logger.getLogger(FreshworksCRMDBHandler.class.getName());

	public FreshworksCRMDBHandler(String collection_name,String client_name) {
		// TODO Auto-generated constructor stub
    uri = new MongoClientURI("mongodb://pre_admin:cz249y(GmP7%239%5EYG@54.211.187.171:27017/?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false");
    mongo = new MongoClient(uri);  
    database = mongo.getDB(client_name);
		collection =  database.getCollection(collection_name);
	}
	
	public void addDocumments(List<JSONObject> docs) {
		List<String> contacts = new ArrayList<>();
		for (int i=0;i < docs.size();i++) {
			JSONObject jso = docs.get(i);
            try {
            	contacts = new ArrayList<>();
				JSONArray jsonarray = jso.getJSONObject("deal").getJSONArray("contact_ids");
				for(int j=0;j< jsonarray.length();j++) {
					String val = jsonarray.get(j).toString();
			    	DBObject doc = database.getCollection("freshsales_contacts").findOne(new BasicDBObject("id", Long.valueOf(val)));
			    	contacts.add(doc.get("email").toString());
				}
                 System.out.println("deal id="+jso.getJSONObject("deal").get("id")+"  contacts="+contacts);
			    DBObject et_ob = database.getCollection("freshsales_etl_deals").findOne(new BasicDBObject("deal_id", Long.valueOf(jso.getJSONObject("deal").get("id").toString())));
                     if(et_ob != null) {
                     et_ob.put("contacts", contacts);
                     et_ob.put("sales_account_id", jso.getJSONObject("deal").get("sales_account_id"));
			    	DBObject stages = database.getCollection("freshsales_deal_stages").findOne(new BasicDBObject("id", Long.valueOf(jso.getJSONObject("deal").get("deal_stage_id").toString())));
			    	et_ob.put("status", stages.get("name"));
			    	DBCursor stages_of_deal = database.getCollection("freshsales_stages_of_deal").find(new BasicDBObject("deal_id", Long.valueOf(jso.getJSONObject("deal").get("id").toString())));
			    	List<DBObject> objects = new ArrayList<>();
			    	while(stages_of_deal.hasNext()) {
			    		 DBObject ob = stages_of_deal.next();
			    		 DBObject cf_presales = (DBObject)JSON.parse(ob.get("custom_field").toString());
			    		 et_ob.put("cf_presales_user", cf_presales.get("cf_presales_user"));
			    		 objects.add(ob);	 
			    	}
			    	if(objects.size() > 0)
			    	et_ob.put("stages", objects);
			    	else 
			    		et_ob.put("stages", null);
			    	//adding or updating
			    	DBObject query1 = new BasicDBObject("deal_id",Long.valueOf(jso.getJSONObject("deal").get("id").toString()));
				    	database.getCollection("freshsales_etl_deals").update(query1, et_ob);
			    }
            }
	 catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
	}
		
		}
		
	}
	
	public List<String> fetchIds() {
        List<String> deals = new ArrayList<String>();
	    Iterator<DBObject> cursor = collection.find().iterator();
       try {
    	   while (cursor.hasNext()) {
    		  // if(status.equalsIgnoreCase("won") || status.equalsIgnoreCase("lost")) {
    		   String id = cursor.next().get("id").toString();
               //TODO: Fill inventory
    		   deals.add(id);
    		  // }
		}
       } catch (Exception e) {
		// TODO: handle exception
    	   e.printStackTrace();
	}
        System.out.println("Deals to process="+deals);
		return deals;
	}
	
	/*
	 * id (Each record has unique id )
	 * Based on this id, Add or updation of documents to MongoCollection
	 * if id not present - Then documents gets Added
	 * if id is present - Then all the fields of documents gets updated.
	 * */
	public void addDocuments(List<JSONArray> docs,String collection_type) {
		for (int i=0;i < docs.size();i++) {
			JSONArray jsonarray = docs.get(i);
			for(int j=0;j< jsonarray.length();j++) {
            try {
				String val = jsonarray.get(j).toString();
				System.out.println(val);
				DBObject dbObject = (DBObject)JSON.parse(val);
	        	DBObject query = new BasicDBObject("id",Long.valueOf(dbObject.get("id").toString()));
				if (collection.count(query) == 0L) {
					LOGGER.info("Adding document:"+Long.valueOf(dbObject.get("id").toString()));
					collection.insert(dbObject);
			    } else {
			    	LOGGER.info("Updating document:"+Long.valueOf(dbObject.get("id").toString()));
			    	collection.update(query, dbObject);
			    }
				//fetching deal status name
		    	DBObject status = database.getCollection("freshsales_deal_stages").findOne(new BasicDBObject("id", dbObject.get("deal_stage_id")));

				//updating stages of deals
				if(collection_type.equals("deals")) {
				List<DBObject> criteria = new ArrayList<DBObject>();
				criteria.add(new BasicDBObject("deal_id", Long.valueOf(dbObject.get("id").toString())));
				criteria.add(new BasicDBObject("stage_id", Long.valueOf(dbObject.get("deal_stage_id").toString())));
				DBCursor dbCursor = database.getCollection("freshsales_stages_of_deal").find(new BasicDBObject("$and", criteria));
			    if(dbCursor.size() == 0) {
			    	BasicDBObject stage_doc = new BasicDBObject();
			    	stage_doc.put("deal_id", dbObject.get("id"));
			    	stage_doc.put("stage_id", dbObject.get("deal_stage_id"));
			    	stage_doc.put("start_time", dbObject.get("stage_updated_time"));
			    	stage_doc.put("custom_field", dbObject.get("custom_field"));
			    	if(status != null) {
			    	stage_doc.put("status",status.get("name"));
			    	} else {
			    		System.out.println("status is null");
			    	}
			    	stage_doc.put("amount", dbObject.get("amount"));
			    	database.getCollection("freshsales_stages_of_deal").insert(stage_doc);
			    	System.out.println(dbObject.get("deal_stage_id") + " inserted");
			    }
			    
			    	//ETL Layer of deal:-
			        //if status is won or lost then no need to perform any operation
			    	//if first time insertion, make note of revenue_while_starting
			    	//while updating, make note of revenue while ending
			    
			//    if(!(status.get("name").toString().equalsIgnoreCase("won") || status.get("name").toString().equalsIgnoreCase("lost"))) {
			    	DBObject query1 = new BasicDBObject("deal_id",Long.valueOf(dbObject.get("id").toString()));
					DBObject et_ob = database.getCollection("freshsales_etl_deals").findOne(new BasicDBObject("deal_id", Long.valueOf(dbObject.get("id").toString())));
					if (et_ob == null) {
						LOGGER.info("ETL Adding document:"+Long.valueOf(dbObject.get("id").toString()));
						et_ob = new BasicDBObject();
						et_ob.put("deal_id", Long.valueOf(dbObject.get("id").toString()));
						et_ob.put("start_time", dbObject.get("created_at"));
						et_ob.put("status", status.get("name"));
						et_ob.put("end_time", dbObject.get("stage_updated_time"));
						et_ob.put("revenue_while_starting",Double.valueOf(dbObject.get("amount").toString()));
				    	database.getCollection("freshsales_etl_deals").insert(et_ob);
				    } else {
						LOGGER.info("ETL Updating document:"+Long.valueOf(dbObject.get("id").toString()));						
						et_ob.put("status", status.get("name"));
						et_ob.put("end_time", dbObject.get("stage_updated_time"));
						et_ob.put("revenue_while_closing",Double.valueOf(dbObject.get("amount").toString()));
				    	database.getCollection("freshsales_etl_deals").update(query1, et_ob);
				    }					
			   // }
			    
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				LOGGER.severe("Exception occured:"+e.toString());
			}
				
			}
		}
	}
	public boolean isrunForviews() {
		if(collection.count() == 0L) {
			return true;
		} else 
			return false;
	}
	
	/*
	 * Retrieve CRM views(module name and id) - To pull data from API. 
	 * */
	public Map<String,String> getViews(Map<String, String> CRMFilters) {
        Map<String,String> views = new HashMap<String, String>();
		try {
		        for (Entry<String, String> element : CRMFilters.entrySet()) {
		        	DBObject query = new BasicDBObject("name",element.getKey());
					DBObject doc = collection.findOne(query);
					views.put(element.getKey(), doc.get("id").toString());
				}
		} catch (Exception e) {
			// TODO: handle exception
			LOGGER.severe("Exception occured:"+e.toString());
		}
		return views;	
	}

}
