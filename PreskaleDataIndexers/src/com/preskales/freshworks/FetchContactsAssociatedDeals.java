package com.preskales.freshworks;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;

import com.preskales.freshworks.DBHandlers.FreshworksCRMDBHandler;

public class FetchContactsAssociatedDeals {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
          fetchData();
	}
	
	public static void fetchData() {
		try {
		String api_url = "https://preskale.freshsales.io/api/deals/";
		FreshworksCRMDBHandler handler = new FreshworksCRMDBHandler("freshsales_deals", "preskale");
        List<String> deals = handler.fetchIds();
        if (deals.size() == 0) {
			return;
		}
		String api_token = "7Cd27g1pWvN1FotaVr2ztw"; // read token key for client
		List<JSONObject> docs = new ArrayList<>();

		for (int i = 0; i < deals.size(); i++) {
			api_url = "https://preskale.freshsales.io/api/deals/" +deals.get(i)+"?include=contacts,sales_account";
			Scanner sc = FreshworksCRM.APIEndpoint(api_url,api_token);
			if (sc == null) {
				throw new Exception("error occured");
			}

			ObjectMapper mapper = new ObjectMapper();

			JSONObject jso_object;
			while (sc.hasNextLine()) {
				String api_content = sc.nextLine();
				Object json = mapper.readValue(api_content, Object.class);
				String indented = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
				jso_object = new JSONObject(indented);
				   docs.add(jso_object);				
			}
		}
		
		handler.addDocumments(docs);
		
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

}
