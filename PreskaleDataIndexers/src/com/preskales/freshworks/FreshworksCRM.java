package com.preskales.freshworks;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.logging.Logger;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;

import com.preskales.freshworks.DBHandlers.FreshworksCRMDBHandler;
/*
 *  This class is used to fetch data from Freshworks CRM data like Leads,Accounts,Deals,Contacts..
 * 
 * */
public class FreshworksCRM {
	private static String crm = "freshsales";
	private static String moduleName = "";
	private static String domainName = "";
	private static String apiPath = "api/";
	private static String view_name = "/view/";
	private static String api_token = "";
	private static String page_path = "?page=";
	private static int current_page_number = 1;
	private static boolean proceed = true;
	private static DefaultHttpClient httpclient = new DefaultHttpClient();
	private static int noofRecords = 0;
	private static boolean runSpecific = false;
	private static boolean runForViews = false;
	private static boolean runForData = false;
	private static List<FreshworksCRMConfig> domainsToprocess = new ArrayList<>();
	private static Map<String, String> CRMfilters = new HashMap<>();
	private static Map<String, String> CRMReferences = new HashMap<>();
    private static String view = "views";
    private static boolean reference_table = false;

	private static final Logger LOGGER = Logger.getLogger(FreshworksCRM.class.getName());

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			LOGGER.info("FreshworksCRM processor started at:-" + new Date());
			for (int i = 0; i < args.length; i++) {
				if (args[i].equals("-Leads")) {
					moduleName = "leads";
				} else if (args[i].equals("-Contacts")) {
					moduleName = "contacts";
				} else if (args[i].equals("-Accounts")) {
					moduleName = "sales_accounts";
				} else if (args[i].equals("-Deals")) {
					moduleName = "deals";
				} else if (args[i].equals("-Sales_activities")) {
					moduleName = "sales_activities";
				} else if (args[i].equals("-runForAll")) {
					// runForAll = true;
				} else if (args[i].equals("-runSpecific")) {
					runSpecific = true;
					domainName = args[i + 1].toString();
				} 
				
			}
			setModules();
			fetchReferences();

			if(new FreshworksCRMDBHandler(crm + "_" + "views", "preskale").isrunForviews()) {
				fetchViews();
			} 
				fetchDomainsToprocess();
				fetchData();
			
		
			LOGGER.info("FreshworksCRM processor Ended at:-" + new Date());
			LOGGER.info("No.of domains processed:-" + domainsToprocess.size());

		} catch (final Exception e) {
			LOGGER.severe("Exception while parsing the main arguments:-" + e.toString());
		}
	}

	/*
	 * Mail alert or slack alert for investingating that CRM account credentials.
	 * */
	public static void triggeralert() {
		// Trigger alert if response is not equal to 200
	}
	
	private static void fetchReferences() {
		domainName = "https://preskale.freshsales.io/"; // Loop every
		api_token = "7Cd27g1pWvN1FotaVr2ztw"; // read token key for client

		final FreshworksCRMConfig config1 = new FreshworksCRMConfig();
		config1.api_token = api_token;
		config1.domain = domainName;
		domainsToprocess.add(new FreshworksCRMConfig(config1));
		List<JSONArray> docs = new ArrayList<>();
		String api_url = "";
		for (FreshworksCRMConfig config : domainsToprocess) {

			for (Entry<String, String> filter : CRMReferences.entrySet()) {
				try {
					LOGGER.info("View name:-" + filter.getKey());
					api_url = config.getDomain() + apiPath + "selector/"+ filter.getKey();
					Scanner sc = APIEndpoint(api_url, config.getApi_token());
					if (sc == null) {
						throw new Exception();
					}
					ObjectMapper mapper = new ObjectMapper();

					JSONObject jso_object;
					while (sc.hasNextLine()) {
						String api_content = sc.nextLine();
						Object json = mapper.readValue(api_content, Object.class);
						String indented = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
						jso_object = new JSONObject(indented);
						docs.add(jso_object.getJSONArray(filter.getValue()));
						new FreshworksCRMDBHandler(crm + "_" + filter.getKey(),config.client_name).addDocuments(docs,filter.getValue());
						docs.clear();
					}
				} catch (Exception e) {
					// TODO: handle exception
					LOGGER.severe("Exception occured:"+e.toString());
				}
			}

		}
		
	}
	
	/*
	 * Filter modules based on main params. So that processor need not run for all the modules.
	 * */
	private static void setModules() {
		switch (moduleName) {
		case "leads":
			CRMfilters.put("All Leads", "leads");
			break;
		case "deals":
			CRMfilters.put("My Deals", "deals");
			break;
			
		case "contacts":
			CRMfilters.put("All Contacts", "contacts");
			break;
		case "sales_accounts":
			CRMfilters.put("All Accounts", "sales_accounts");
			break;
		default:
			CRMfilters.put("All Contacts", "contacts");
	//		CRMfilters.put("All Leads", "leads");
			CRMfilters.put("Recent Deals", "deals");
	//		CRMfilters.put("All Accounts", "sales_accounts");
			break;
		}
//		CRMReferences.put("owners", "users");
//		CRMReferences.put("territories", "territories");
//		CRMReferences.put("deal_stages", "deal_stages");
//		CRMReferences.put("deal_reasons", "deal_reasons");
//		CRMReferences.put("deal_types", "deal_types");
//		CRMReferences.put("sales_activity_types", "sales_activity_types");
//		CRMReferences.put("sales_activity_outcomes", "sales_activity_outcomes");
//		CRMReferences.put("sales_activity_entity_types", "sales_activity_entity_types");

		
	}

	private static void fetchViews() {
		domainName = "https://preskale.freshsales.io/"; // Loop every
		api_token = "7Cd27g1pWvN1FotaVr2ztw"; // read token key for client

		final FreshworksCRMConfig config1 = new FreshworksCRMConfig();
		config1.api_token = api_token;
		config1.domain = domainName;
		domainsToprocess.add(new FreshworksCRMConfig(config1));
		List<JSONArray> docs = new ArrayList<>();
		String api_url = "";
		for (FreshworksCRMConfig config : domainsToprocess) {

			for (Entry<String, String> filter : CRMfilters.entrySet()) {
				try {
					LOGGER.info("View name:-" + filter.getValue());
					api_url = config.getDomain() + apiPath + filter.getValue() + "/filters";
					Scanner sc = APIEndpoint(api_url, config.getApi_token());
					if (sc == null) {
						throw new Exception();
					}
					ObjectMapper mapper = new ObjectMapper();

					JSONObject jso_object;
					while (sc.hasNextLine()) {
						String api_content = sc.nextLine();
						Object json = mapper.readValue(api_content, Object.class);
						String indented = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
						jso_object = new JSONObject(indented);
						docs.add(jso_object.getJSONArray("filters"));
						new FreshworksCRMDBHandler(crm + "_" + view,config.client_name).addDocuments(docs,filter.getValue());
						docs.clear();
					}
				} catch (Exception e) {
					// TODO: handle exception
					LOGGER.severe("Exception occured:"+e.toString());
				}
			}

		}

	}

	private static void fetchDomainsToprocess() {
		 domainsToprocess.clear();
		if (runSpecific) {
			domainName = "https://preskale.freshsales.io/"; // Loop every
			api_token = "7Cd27g1pWvN1FotaVr2ztw"; // read token key for client

			final FreshworksCRMConfig config = new FreshworksCRMConfig();
			config.api_token = api_token;
			config.domain = domainName;
			config.CRMViews = new FreshworksCRMDBHandler(crm + "_" + view,config.client_name).getViews(CRMfilters);
			domainsToprocess.add(new FreshworksCRMConfig(config));
		} else {
			domainName = "https://preskale.freshsales.io/"; // Loop every
			api_token = "7Cd27g1pWvN1FotaVr2ztw"; // read token key for client

			final FreshworksCRMConfig config = new FreshworksCRMConfig();
			config.api_token = api_token;
			config.domain = domainName;
			config.CRMViews = new FreshworksCRMDBHandler(crm + "_" + view,config.client_name).getViews(CRMfilters);
			domainsToprocess.add(new FreshworksCRMConfig(config));
		}
	}

	public static Scanner APIEndpoint(String api_url, String api_token) {
		try {
			LOGGER.info("Endpoint:"+api_url);
			// Creating a HttpGet object
			HttpGet httpget = new HttpGet(api_url);
			httpget.addHeader("Authorization", "Token token=" + api_token);
			// Printing the method used
			LOGGER.info("Request Type: " + httpget.getMethod());
			HttpResponse httpresponse = httpclient.execute(httpget);
			// check the http status
			if (httpresponse.getStatusLine().getStatusCode() != 200) {
				// if response code is not 200, then something happened
				// in the account.need to reinvestigate the account
				// settings.
				LOGGER.info(httpresponse.getStatusLine() + "Domain:-" + domainName);
				triggeralert();
			} else {
				LOGGER.info("Response code:-" + httpresponse.getStatusLine().getStatusCode());
			}
			Scanner sc = new Scanner(httpresponse.getEntity().getContent());
			return sc;

		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}

	private static void fetchData() {
		List<JSONArray> docs = new ArrayList<>();
		for (FreshworksCRMConfig config : domainsToprocess) {
			LOGGER.info("processing domain:-" + config.getDomain());
			for (Entry<String, String> filter : CRMfilters.entrySet()) {
				LOGGER.info("Module:- "+filter.getKey());
			docs.clear();
			proceed = true;
			noofRecords = 0;
			current_page_number = 1;
			while (proceed) {
				try {
					// Creating a HttpClient object
					String api_url = config.getDomain() + apiPath + filter.getValue() + view_name + config.getCRMViews().get(filter.getKey())
					+ page_path + current_page_number;
					// Creating a HttpGet object
					Scanner sc = APIEndpoint(api_url, config.getApi_token());
					if (sc == null) {
						throw new Exception("error occured");
					}

					ObjectMapper mapper = new ObjectMapper();

					JSONObject jso_object;
					while (sc.hasNextLine()) {
						String api_content = sc.nextLine();
						Object json = mapper.readValue(api_content, Object.class);
						String indented = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
						jso_object = new JSONObject(indented);
						docs.add(jso_object.getJSONArray(filter.getValue()));
						// CRM pagination:- 25 records per API call.
						noofRecords = noofRecords + jso_object.getJSONArray(filter.getValue()).length();
						int page = jso_object.getJSONObject("meta").getInt("total_pages");
						if (page > current_page_number) {
							current_page_number++; // proceed next page
						} else {
							proceed = false;
						}
					}

				} catch (Exception e) {
					// TODO: handle exception
					LOGGER.severe("Error occured:"+e.toString());
				}

			}
			LOGGER.info("No of records Fetched from api:-" + noofRecords);
			new FreshworksCRMDBHandler(crm + "_" + filter.getValue(),config.client_name).addDocuments(docs,filter.getValue());

		}
		}
	}

}

class FreshworksCRMConfig {
	String domain;
	String client_name = "preskale";
	Map<String, String> CRMViews = new HashMap<>();	
	String api_token;

	public FreshworksCRMConfig() {
		// TODO Auto-generated constructor stub
	}

	public FreshworksCRMConfig(FreshworksCRMConfig obj) {
		// TODO Auto-generated constructor stub
		super();
		this.domain = obj.domain;
		this.CRMViews = obj.CRMViews;
		this.api_token = obj.api_token;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	

	public String getApi_token() {
		return api_token;
	}

	public void setApi_token(String api_token) {
		this.api_token = api_token;
	}

	public Map<String, String> getCRMViews() {
		return CRMViews;
	}

	public void setCRMViews(Map<String, String> cRMViews) {
		CRMViews = cRMViews;
	}

}
