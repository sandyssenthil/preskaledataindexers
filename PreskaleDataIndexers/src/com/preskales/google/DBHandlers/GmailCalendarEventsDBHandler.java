package com.preskales.google.DBHandlers;

import java.util.List;
import java.util.logging.Logger;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

public class GmailCalendarEventsDBHandler {
	private static MongoClient mongo;
	private static MongoClientURI uri;
	private static DB database;
	private static DBCollection collection;
	//private static DBCollectionUpdateOptions update_options = new DBCollectionUpdateOptions();
	private final static Logger LOGGER = Logger.getLogger(GmailConversionDBHandler.class.getName());

	public GmailCalendarEventsDBHandler(String collection_name,String client_name) {
		// TODO Auto-generated constructor stub
    uri = new MongoClientURI("mongodb://pre_admin:cz249y(GmP7%239%5EYG@54.211.187.171:27017/?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false");
    mongo = new MongoClient(uri);  
    database = mongo.getDB(client_name);
		collection =  database.getCollection(collection_name);
	}
	/*
	 * id (Each record has unique id )
	 * Based on this id, Add or updation of documents to MongoCollection
	 * if id not present - Then documents gets Added
	 * if id is present - Then all the fields of documents gets updated.
	 * */
	public void addDocuments(List<BasicDBObject> docs) {
		for (int i=0;i < docs.size();i++) {
			BasicDBObject dbObject = docs.get(i);
            try {
				DBObject query = new BasicDBObject("event_id",dbObject.get("event_id"));
				if (collection.count(query) == 0L) {
					LOGGER.info("Adding document:"+dbObject.get("event_id"));
					collection.insert(dbObject);
			    } 
				else {
			    	LOGGER.info("Updating document:"+dbObject.get("event_id"));
			    	collection.update(query, dbObject);
			    }	
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				
			}
		}
	public void addTasks(List<BasicDBObject> docs) {
		for (int i=0;i < docs.size();i++) {
			BasicDBObject dbObject = docs.get(i);
            try {
				DBObject query = new BasicDBObject("task_id",dbObject.get("task_id"));
				if (collection.count(query) == 0L) {
					LOGGER.info("Adding document:"+dbObject.get("task_id"));
					collection.insert(dbObject);
			    } 
				else {
			    	LOGGER.info("Updating document:"+dbObject.get("task_id"));
			    	collection.update(query, dbObject);
			    }	
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				
			}
	}
	public void addUser(List<BasicDBObject> docs) {
		for (int i=0;i < docs.size();i++) {
			BasicDBObject dbObject = docs.get(i);
            try {
				DBObject query = new BasicDBObject("user_id",dbObject.get("user_id"));
				if (collection.count(query) == 0L) {
					LOGGER.info("Adding document:"+dbObject.get("user_id"));
					collection.insert(dbObject);
			    } 
				else {
			    	LOGGER.info("Updating document:"+dbObject.get("user_id"));
			    	collection.update(query, dbObject);
			    }	
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				
			}
	}
}
