package com.preskales.google.DBHandlers;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

public class GmailConversionDBHandler {
	private static MongoClient mongo;
	private static MongoClientURI uri;
	private static DB database;
	private static DBCollection collection;
	//private static DBCollectionUpdateOptions update_options = new DBCollectionUpdateOptions();
	private final static Logger LOGGER = Logger.getLogger(GmailConversionDBHandler.class.getName());

	public GmailConversionDBHandler(String collection_name,String client_name) {
		// TODO Auto-generated constructor stub
    uri = new MongoClientURI("mongodb://pre_admin:cz249y(GmP7%239%5EYG@54.211.187.171:27017/?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false");
    mongo = new MongoClient(uri);  
    database = mongo.getDB(client_name);
		collection =  database.getCollection(collection_name);
	}
	/*
	 * id (Each record has unique id )
	 * Based on this id, Add or updation of documents to MongoCollection
	 * if id not present - Then documents gets Added
	 * if id is present - Then all the fields of documents gets updated.
	 * */
	public void addDocuments(List<BasicDBObject> docs) {
		for (int i=0;i < docs.size();i++) {
			BasicDBObject dbObject = docs.get(i);
            try {
				DBObject query = new BasicDBObject("message_id",dbObject.get("message_id"));
				if (collection.count(query) == 0L) {
					LOGGER.info("Adding document:"+dbObject.get("message_id"));
					collection.insert(dbObject);
			    } 
				else {
			    	LOGGER.info("Updating document:"+dbObject.get("message_id"));
			    	collection.update(query, dbObject);
			    }	
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				
			}
		}
	public void addConversation(List<BasicDBObject> docs) {
		for (int i=0;i < docs.size();i++) {
			BasicDBObject dbObject = docs.get(i);
            try {
				DBObject query = new BasicDBObject("conversation_id",dbObject.get("conversation_id"));
		    	DBObject ob = collection.findOne(new BasicDBObject("conversation_id",dbObject.get("conversation_id").toString()));

				if (ob == null) {
					LOGGER.info("Adding conversation:"+dbObject.get("conversation_id"));
					collection.insert(dbObject);
			    } 
				else {
			    	LOGGER.info("Updating conversation:"+dbObject.get("conversation_id"));
			    	ob.put("messages_count", dbObject.get("messages_count"));
			    	ob.put("conversations", dbObject.get("conversations"));
			    	ob.put("updated_at", new Date());
			    	collection.update(query, ob);
			    }	
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				
			}
		}
	

}
