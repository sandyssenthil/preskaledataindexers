package com.preskale.testmodule;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mongodb.BasicDBObject;
import com.preskales.google.DBHandlers.GmailCalendarEventsDBHandler;

public class AddUser {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
          addUserToMongo();
	}
	
	/*
	 * {
		"user_id": String,
		"user_name": String,
		"image": blob,
		"connections": [
			{
				"type": String,
				"token_details": {
					"access_token": String,
					"refresh_token": String
				}
			},
			{
				"type": String,
				"token_details": {
					"access_token": String,
					"refresh_token": String
				}
			}
		]
	}*/
	
	private static  void addUserToMongo() {
		
		List<BasicDBObject> docs = new ArrayList<BasicDBObject>();
	    List<BasicDBObject> connections = new ArrayList<>();

		BasicDBObject doc = new BasicDBObject();
		doc.put("user_id", 1);
		doc.put("user_name", "senthil@preskale.com");
		doc.put("image", null);
		doc.put("is_periodic", true);
		
		BasicDBObject con1 = new BasicDBObject();
		con1.put("type", "google");
		
        BasicDBObject con1_sub = new BasicDBObject();
        con1_sub.put("access_token", "ya29.a0AfH6SMDrGJEYsi3EPeqzSmkrGrFX-NdCfHyVcX48jQGw961ovP5x-QZTknItKeH2-GjZJRS5W3EsLOYMEJHBGpmkhB4nbO58krK6Hx62dBHFpA-domPTSks91DAzNxNnJYjo3UrgAQZCSJOJ2r9Yv2rlHcq4698BhOw");
        con1_sub.put("expires_in", 3599);
        con1_sub.put("refresh_token", "1//0gtJCnVXZYBCRCgYIARAAGBASNwF-L9IrXahml1BClBt3b4Q-eyfhIuk2h_adrYbLNq8QJsXN7Uxv2Nfvc0FYBqnVDOui28jS9RI");
        con1_sub.put("token_type", "bearer");
        con1_sub.put("last_token_refreshed", new Date());
        con1_sub.put("last_data_pulled", new Date());
        
        con1.put("token_details", con1_sub);
        

       connections.add(con1);        
        
        
        BasicDBObject con2 = new BasicDBObject();
		con2.put("type", "freshworks");
		
        BasicDBObject con2_sub = new BasicDBObject();
        con2_sub.put("access_token", "ya29.a0AfH6SMDrGJEYsi3EPeqzSmkrGrFX-NdCfHyVcX48jQGw961ovP5x-QZTknItKeH2-GjZJRS5W3EsLOYMEJHBGpmkhB4nbO58krK6Hx62dBHFpA-domPTSks91DAzNxNnJYjo3UrgAQZCSJOJ2r9Yv2rlHcq4698BhOw");
        con2_sub.put("expires_in", 3599);
        con2_sub.put("refresh_token", "1//0gtJCnVXZYBCRCgYIARAAGBASNwF-L9IrXahml1BClBt3b4Q-eyfhIuk2h_adrYbLNq8QJsXN7Uxv2Nfvc0FYBqnVDOui28jS9RI");
        con2_sub.put("token_type", "bearer");
        con2_sub.put("last_token_refreshed", new Date());
        con2_sub.put("last_data_pulled", new Date());
        
        
        con2.put("token_details", con2_sub);
        
        connections.add(con2); 
        
        doc.put("connections", connections);
        
        
        docs.add(doc);
        GmailCalendarEventsDBHandler handler = new GmailCalendarEventsDBHandler("users", "preskale");
        handler.addUser(docs);
		
		
		
		
	}

}
